package wm1819.cse.chalmers.se.livedemoapp;

import android.support.test.filters.MediumTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.EditText;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@MediumTest
@RunWith(AndroidJUnit4.class)
public class MyActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> rule  = new  ActivityTestRule<>(MainActivity.class);

    @Test
    public void ensureTextViewIsCorrect() throws Exception {
        MainActivity activity = rule.getActivity();
        EditText textView = activity.findViewById(R.id.editText);
        assertEquals("test",textView.getText().toString());
    }
}
